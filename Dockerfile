FROM python

WORKDIR /aff_test
COPY requirements.txt .
RUN pip install -r requirements.txt

CMD pytest -s --alluredir=allure-results/ /aff_test/tests