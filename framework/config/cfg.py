import yaml

class Config:
    """cohfifg"""

    def __init__(self, file_name):
        """init"""
        with open(file_name, 'r') as f:
            self.__config = yaml.safe_load(f)
    
    def get_config(self):
        """get_config"""
        return self.__config