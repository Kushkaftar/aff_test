from urllib.parse import urlunparse, urlparse
from urllib import parse

class URL_Helper:
    """url helper class"""

    def __init__(self, scheme, host, path):
        """set base url"""

        data = [scheme, host, path, '', '', '',]
        self.__base_url = urlunparse(data)
    
    def add_path(self, path_part):
        """add path"""

        u = urlparse(self.__base_url)
        new_url = u._replace(path = u.path + path_part)
        # new_url = new_url._replace(query = data_query)

        return new_url.geturl()
    
    @staticmethod
    def get_query_param(url):
        """get dict query parameters"""

        query = parse.parse_qsl(parse.urlsplit(url).query)

        return dict(query)