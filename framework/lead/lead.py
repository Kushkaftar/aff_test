from faker import Faker
from faker.providers import user_agent

class Lead:
    """Lead object"""
    r_id = ''
    landing_uuid = ''
    lead_uuid = ''


    def __init__(self, flow_uuid, ip, country_code):
        fake = Faker('ru_RU')
        fake.add_provider(user_agent)

        self.__flow_uuid = flow_uuid
        self.__ip = ip
        self.__country_code = country_code

        self.__name= fake.name()
        self.__user_agent = fake.user_agent()

    def get_flow_uuid(self):
        return self.__flow_uuid

    @classmethod
    def set_r_id(cls, r_id):
        cls.r_id = r_id
    
    @classmethod
    def get_r_id(cls):
        return cls.r_id
    
    @classmethod
    def set_landing_uuid(cls, landing_uuid):
        cls.landing_uuid = landing_uuid
    
    @classmethod
    def get_landing_uuid(cls):
        return cls.landing_uuid
    
    @classmethod
    def set_lead_uuid(cls, lead_uuid):
        cls.lead_uuid = lead_uuid
    
    @classmethod
    def get_lead_uuid(cls):
        return cls.lead_uuid