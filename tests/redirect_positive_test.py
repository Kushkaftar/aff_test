import requests
import allure

from framework.config.cfg import Config
from framework.lead.lead import Lead
from framework.url_helper.url_helper import URL_Helper


class TestRedirectPositive:
    """positive suite redirect"""

    def setup(self):
        """setup"""
        # TODO: функция вызывается для каждого теста, переделать на фикстуру
        c = Config('configs/dev.yaml')
        self.__config = c.get_config()
        
        self.base_url = URL_Helper(self.__config['api_settings']['scheme'], self.__config['api_settings']['redirect_host'], '/r/')
        self.lead = Lead(self.__config['publisher']['flow_uuid'], '95.179.27.252', 'RU')

    # @allure.step('get r_id from redirect to flow link')
    def test_get_r_id_from_redirect(self):
        """test get r_id from redirect to flow link"""

        flow = self.lead.get_flow_uuid()
        url_redirect = self.base_url.add_path(flow)
        allure.attach('url redirect attach', url_redirect)

        resp = requests.get(url_redirect, allow_redirects=False)
        allure.attach('status_code', resp.status_code)

        loc = resp.headers['Location']
        allure.attach('headers Location', loc)

        query = self.base_url.get_query_param(loc)

        assert resp.status_code == 302
        assert query['r_id'].strip() != ''
        assert query['country_code'].strip() != ''

        self.lead.set_r_id(query['r_id'])
    
    # @allure.step('get link to land')
    def test_get_link_to_land(self):
        """Test get link to land"""

        url = self.base_url.add_path('url') + '?r_id=' + self.lead.get_r_id()
        allure.attach('url get link to land', url)

        resp = requests.get(url, allow_redirects=False)

        assert resp.status_code == 200

        data = resp.json()
        allure.attach('response json', data)

        assert data['success'] == True
        assert data['data']['url'].strip() != ''

        land_url = data['data']['url']
        query = self.base_url.get_query_param(land_url)

        assert query['r_id'].strip() != ''
        assert query['landing_uuid'].strip() != ''
        assert query['r_id'] == self.lead.get_r_id()

        self.lead.set_landing_uuid(query['landing_uuid'])
    

    # @allure.step('update visit to land')
    def test_update_visit_to_land(self):
        """Update visit to land."""

        url = self.base_url.add_path('update')
        allure.attach('url update visit to land', url)

        headers = {'Content-Type': 'application/json'}

        json_data = {
            "r_id": self.lead.get_r_id(),
            "landing_uuid": self.lead.get_landing_uuid(),
        }

        resp = requests.post(url= url, headers = headers, json = json_data)
        
        assert resp.status_code == 200
        data = resp.json()
        allure.attach('response json', data)

        assert data['success'] == True

    # @allure.step('create lead')
    def test_create_lead(self):
        """test create lead"""

        url = "https://api.devcpa.net/l/create"
        allure.attach('url create lead', url)

        headers = {'Content-Type': 'application/json'}

        json_data = {
            "r_id": self.lead.get_r_id(),
            "country_code": "RU",
            "phone": "89100000000",
            "name": "test"
        }

        resp = requests.post(url= url, headers = headers, json = json_data)
        print(resp.json())
        assert resp.status_code == 200

        data = resp.json()
        allure.attach('response json', data)

        assert data['success'] == True
        assert data['data']['lead_uuid'].strip() != ''